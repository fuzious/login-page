package source;
import java.io.IOException;
import java.io.PrintWriter;
import javax.servlet.ServletContext;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;

@WebServlet("/servlet1")
public class servlet1 extends HttpServlet{
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException,IOException{
        PrintWriter out=response.getWriter();
        HttpSession sess=request.getSession();
        String uname=(String)sess.getAttribute("username");
        String password=(String)sess.getAttribute("password");
        if(password.equals("Arpit")) {
            out.println("Welcome " + uname);
            out.close();
        }
        else {
            sess.setAttribute("correct","false");
            ServletContext sc = getServletContext();
            sc.getRequestDispatcher("/index.jsp").forward(request, response);
        }
    }
}
